import az.div.restraunt.Drink;
import az.div.restraunt.Meal;
import az.div.restraunt.Menu;
import az.div.restraunt.Order;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Menu menu = new Menu();
        Meal meat = new Meal("Kabab", 6.0);
        menu.addMeal(meat);

        Meal salad = new Meal("Sezar", 12);
        menu.addMeal(salad);
        Meal fish = new Meal("Qizil baliq", 30);
        menu.addMeal(fish);

        Drink tea = new Drink("Soyuq Cay", 5);
        menu.addDrink(tea);
        Drink water = new Drink("Sirab", 2);
        menu.addDrink(water);
        Drink coffee = new Drink("Latte", 5);
        menu.addDrink(coffee);

        System.out.println("Yemekler");
        for (Meal meal : menu.getMeals()) {
            System.out.println(meal.getName() + " " + meal.getPrice());
        }


        System.out.println("Ickiler");
        for (Drink drink : menu.getDrinks()) {
            System.out.println(drink.getName() + " " + drink.getPrice());
        }


        /// Add Order orderNo
        //totalPrice
        //List<Meal>=new ArrayList<>();
        //List<Drink>


        Order order1=new Order(meal);
        Order order2=new Order(meal);


    }
}