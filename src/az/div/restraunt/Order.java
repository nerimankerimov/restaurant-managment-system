package az.div.restraunt;

import java.util.List;

public class Order {
    private List<Meal> meals;
    private List<Drink> drinks;
    private int orderNo;
    private double totalPrice;

    public Order(List<Meal> meals, List<Drink> drinks, int orderNo, double totalPrice) {
        this.meals = meals;
        this.drinks = drinks;
        this.orderNo = orderNo;
        this.totalPrice = totalPrice;
    }
}
